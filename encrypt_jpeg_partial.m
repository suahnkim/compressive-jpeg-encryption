function [partially_encrypted_image_object]=encrypt_jpeg_partial(image_object,key,region)
%Works only for bw image
partially_encrypted_image_object=image_object;
if size(image_object.coef_arrays,2)==1
    %add path
    addpath('encryption_function');
    addpath('decryption_function');
    
    jpeg_quant = image_object.quant_tables{image_object.comp_info(1).quant_tbl_no};
    partially_encrypted_image_object.coef_arrays{1}=pred_encrypt(image_object.coef_arrays{1},jpeg_quant,key,region);
    else
    disp('Only works for bw image')
end

end



function [partially_encrypted_image]=pred_encrypt(o_coeff,DCTQ,key,region)
%decryption check: 1 = enable, 0 = disable
decryption_check=0;
%test check: 1 = enable, 0 = disable
test_check=0;
%step by step encryption: 1 = enable, 0 = disable
step_by_step = 0;

%Encryption applies only to the region: region contains 2 block positions
%(rectangle coordiantes top left, bottom right as a row vector)
%region=[y-coordinate x-coordinate y-coordinate x-coordinate]




%% Prediction
[h, w]=size(o_coeff);
reconstructed_pixels=zeros(h,w);
p_coeff=o_coeff;
p_coeff_existing=o_coeff;
dctmtx_8=dctmtx(8);
for i=1:8:h
    for j=1:8:w
        reconstructed_pixels(i:i+7,j:j+7)=round(dctmtx_8'*(o_coeff(i:i+7,j:j+7).*DCTQ)*dctmtx_8)+128;
        q_block=o_coeff(i:i+7,j:j+7);
        q_block(1,1)=0;
        p_reconstructed_pixels(i:i+7,j:j+7)=round(dctmtx_8'*(q_block.*DCTQ)*dctmtx_8)+128;
    end
end

counter=0;
previous_dc=0;
previous_c_coeff2=0;
for i=1:8:h
    for j=1:8:w
        current_p=p_reconstructed_pixels(i:i+7,j:j+7);
        if i==1 && j ==1
            %no prediction
            DC_prediction=0;
            DC_prediction_existing=0;
            
            AC_0_1_prediction=0;
            AC_1_0_prediction=0;
            DC_prediction_test=0;
        elseif i ==1 %horizontal
            %use W's right pixels to predict
            W=double(reconstructed_pixels(i:i+7,j-8:j-1));
            DC_prediction=round(sum(W(1:end,8)-current_p(1:end,1))/DCTQ(1,1));
            DC_prediction_existing=round(8*(sum(W(1:end,8))/8-128)/DCTQ(1,1));
            
            AC_0_1_prediction=0;
            AC_1_0_prediction=0;
            DC_prediction_test=0;
            
        elseif j ==1 %vertical
            %use N's bottom row pixels to predict
            N=double(reconstructed_pixels(i-8:i-1,j:j+7));
            DC_prediction=round(sum(N(8,1:end)-current_p(1,1:end))/DCTQ(1,1));
            DC_prediction_existing=round((sum(N(8,1:end))/8-128)/DCTQ(1,1));
            
            AC_0_1_prediction=0;
            AC_1_0_prediction=0;
            DC_prediction_test=0;
        else
            %use top + west
            N=double(reconstructed_pixels(i-8:i-1,j:j+7));
            W=double(reconstructed_pixels(i:i+7,j-8:j-1));
            DC_prediction=round((sum(W(1:end,8)-current_p(1:end,1))+sum(N(8,1:end)-current_p(1,1:end)))/DCTQ(1,1)/2);
            DC_prediction_existing=round(8*((sum(W(1:end,8))+sum(N(8,1:end)))/16-128)/DCTQ(1,1));
            
            %test DC prediction
            DC_prediction_test=round(8*((sum(reconstructed_pixels(i-1,j-1)+W(1,8)+N(8,1)))/3-128)/DCTQ(1,1));
            
            %test AC_0_1, AC_1_0 prediction
            temp=4*sqrt(2)./cos((2*([0 1 2 3 4 5 6 7])+1)*pi/16);
            
            %             [zeros(8) N_p ; W_p current_p]
            N_p=double(p_reconstructed_pixels(i-8:i-1,j:j+7))-128;
            W_p=double(p_reconstructed_pixels(i:i+7,j-8:j-1))-128;
            q_block=o_coeff(i:i+7,j:j+7);
            q_block(1,1)=0;
            %             q_block(1,2)=0;
            q_block(2,1)=0;
            p_reconstructed_pixels_AC_0_1_1_0=round(dctmtx_8'*(q_block.*DCTQ)*dctmtx_8);
            AC_0_1_prediction=round(sum(temp.*(N_p(8,1:end)-p_reconstructed_pixels_AC_0_1_1_0(1,1:end)))/8/DCTQ(1,2));
            AC_1_0_prediction=round(sum(temp*W_p(1:end,8))/8/DCTQ(2,1));
            %test
            
            
        end
        
        counter=counter+1;
        
        %existing work
        p_coeff_existing(i,j)=o_coeff(i,j)-DC_prediction_existing;
        %         e_coeff_existing(i,j)=o_coeff(i,j)-DC_prediction;
        %         e_coeff_existing(i,j)=(-1)^(mod(counter,2)+1)*e_coeff_existing(i,j)+previous_c_coeff2;
        e_coeff_existing_list(counter)=p_coeff_existing(i,j);
        %         previous_c_coeff2=e_coeff_existing(i,j);
        
        %Proposed
        p_coeff(i,j)=o_coeff(i,j)-DC_prediction;
        e_coeff_list(counter)=p_coeff(i,j);
        
        %Proposed AC prediction
        %         e_coeff(i,j+1)=o_coeff(i,j+1)-AC_0_1_prediction;
        %         e_coeff(i+1,j)=o_coeff(i+1,j)-AC_1_0_prediction;
        o_coeff_AC_0_1_list(counter)=o_coeff(i,j+1);
        o_coeff_AC_1_0_list(counter)=o_coeff(i+1,j);
        e_coeff_AC_0_1_list(counter)=o_coeff(i,j+1)-AC_0_1_prediction;
        e_coeff_AC_1_0_list(counter)=o_coeff(i+1,j)-AC_1_0_prediction;
        
        %Proposed DC_test
        e_coeff_test(i,j)=o_coeff(i,j)-DC_prediction_test;
        e_coeff_test_list(counter)=e_coeff_test(i,j);
        
        %regular DPCM
        o_coeff_list(counter)=o_coeff(i,j)-previous_dc;
        previous_dc=o_coeff(i,j);
    end
end

[num_regions,~]=size(region);
temp_encrypted_image=p_coeff;
partially_encrypted_image=o_coeff;
for counter=1:num_regions
    region_top_left=region(counter,1:2);
    region_bottom_right=region(counter,3:4);
    region_y1=8*(region_top_left(1)-1)+1;
    region_y2=8*(region_bottom_right(1)-1)+8;
    region_x1=8*(region_top_left(2)-1)+1;
    region_x2=8*(region_bottom_right(2)-1)+8;
    encrypt_region=temp_encrypted_image(region_y1:region_y2,region_x1:region_x2);

    %% Encryption
    temp_encrypted_image(region_y1:region_y2,region_x1:region_x2) = format_preserving_encryption(encrypt_region,DCTQ,key);
    partially_encrypted_image(region_y1:region_y2,region_x1:region_x2) = temp_encrypted_image(region_y1:region_y2,region_x1:region_x2);
end
end

function undo_dpcm_e_coeff = format_preserving_encryption(p_coeff,DCTQ,key)
test_check=0; %for debugging
decryption_check=0; %for debugging
[h, w]=size(p_coeff);

%% 1. encryption: substitution (s)
[e_coeff_s]=substitution(p_coeff,DCTQ,key);

%Check for decryption
if decryption_check ==1
    re_e_coeff=d_substitution(e_coeff_s,DCTQ,key);
    isequal(p_coeff,re_e_coeff)
end


%% 2. encryption: AC inter_intra permutation (iip)
[e_coeff_s_iip]=inter_intra_permutation(e_coeff_s,key);

if test_check==1
    [e_coeff_iip]=inter_intra_permutation(p_coeff,key);
end

%Check for decryption
if decryption_check ==1
    [re_e_coeff_s]=d_inter_intra_permutation(e_coeff_s_iip,key);
    isequal(re_e_coeff_s,e_coeff_s)
end

%% 3. encryption: AC inter_permutation (acip)
[e_coeff_s_iip_acip]=AC_inter_permutation(e_coeff_s_iip,key);

if test_check==1
    [e_coeff_acip]=AC_inter_permutation(p_coeff,key);
end

%check for decryption
if decryption_check ==1
    [re_e_coeff_s_iip]=d_AC_inter_permutation(e_coeff_s_iip_acip,key);
    isequal(re_e_coeff_s_iip,e_coeff_s_iip)
end

%% 4. encryption: DC inter_permutation (dcip)
e_coeff_s_iip_acip_dcip=e_coeff_s_iip_acip;
e_coeff_s_iip_acip_dcip(1:8:h,1:8:w)=inter_permutation(e_coeff_s_iip_acip(1:8:h,1:8:w),key);

if test_check==1
    e_coeff_dcip=p_coeff;
    e_coeff_dcip(1:8:h,1:8:w)=inter_permutation(p_coeff(1:8:h,1:8:w),key);
end

%Check for decryption
if decryption_check ==1
    re_e_coeff_s_dcip=e_coeff_s_iip_acip_dcip;
    re_e_coeff_s_dcip(1:8:h,1:8:w)=d_inter_permutation(e_coeff_s_iip_acip_dcip(1:8:h,1:8:w),key);
    isequal(e_coeff_s_iip_acip,re_e_coeff_s_dcip)
end

%% 5. encryption: AC intra_permutation (intra)
e_coeff_s_iip_acip_dcip_intra=intra_permutation(e_coeff_s_iip_acip_dcip,key);

if test_check==1
    e_coeff_intra=intra_permutation(e_coeff_s,key);
end

%check for decryption
if decryption_check ==1
    re_e_coeff_s_iip_acip_dcip=d_intra_permutation(e_coeff_s_iip_acip_dcip_intra,key);
    isequal(e_coeff_s_iip_acip_dcip,re_e_coeff_s_iip_acip_dcip)
end

%Pre processing to undo the effect of DPCM (jpeg_toolbox implements DPCM)
undo_dpcm_e_coeff=e_coeff_s_iip_acip_dcip_intra;
undo_dpcm_e_coeff(1:8:h,1:8:w)=reshape(cumsum(reshape(e_coeff_s_iip_acip_dcip_intra(1:8:h,1:8:w)',1,[])),size(e_coeff_s_iip_acip_dcip_intra(1:8:h,1:8:w),2),[])'; %need to transpose before reshaping to make sure the dc positions are aligned correctly
end

