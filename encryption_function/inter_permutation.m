function e_coeff_p=inter_permutation(e_coeff,key)

%coefficient dependent seed creation
coeff_size=ceil(log2(abs(e_coeff)+1));
seed=key+sum(sum(coeff_size));
rng(seed); e_coeff_size=size(e_coeff);

%permutation key
permutation_key=randperm(e_coeff_size(1)*e_coeff_size(2));

%permute
e_coeff_p=e_coeff(reshape(permutation_key,e_coeff_size(1), e_coeff_size(2)));
end