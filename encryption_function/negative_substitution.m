function e_coeff_s=negative_substitution(e_coeff,DCTQ,key)
%coefficient dependent seed creation
coeff_size=ceil(log2(abs(e_coeff)+1));
seed=key+sum(sum(coeff_size));
rng(seed);

%generate key stream
max_coeff_value=ceil(2*1024/min(min(DCTQ))); %Used to determine the range of quantized DCT coefficients
key_stream = randi([0,max_coeff_value],sum(sum(coeff_size>0)),1);
coeff_size_pos=coeff_size>0;

e_coeff_s=e_coeff;
e_coeff_s(coeff_size_pos)=(-1).^(mod(key_stream,2)).*e_coeff_s(coeff_size_pos);

%save the results
% coeff_size_s=ceil(log2(abs(e_coeff_s)+1));
% isequal(sum(coeff_size,'all'),sum(coeff_size_s,'all'))
end