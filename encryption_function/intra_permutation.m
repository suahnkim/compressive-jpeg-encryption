function e_coeff=intra_permutation(o_coeff,key)
% organize AC in terms of their encoded symbols
z_pattern=[...
    9 2 3 10 17 25 18 11 4 5 12 19 26 ...
    33 41 34 27 20 13 6 7 14 21 28 35 ...
    42 49 57 50 43 36 29 22 15 8 16 23 ...
    30 37 44 51 58 59 52 45 38 31 24 32 ...
    39 46 53 60 61 54 47 40 48 55 62 63 56 64];
[~,i_z_pattern]=sort(z_pattern);

k=0;
[h,w] = size(o_coeff);
zig_zag_ac=zeros(63,h*w/64);

%seed generation
coeff_size=ceil(log2(abs(o_coeff)+1));
seed=key+sum(sum(coeff_size));

for i=1:8:h
    for j=1:8:w
        k=k+1;
        QC=o_coeff(i:i+7,j:j+7);
        zig_zag_ac_row=reshape(QC(z_pattern),[],1);
        zig_zag_ac(1:63,k) = zig_zag_ac_row;
        
        %apply permutation for every non-zero AC element in the block
        non_zero_AC_pos = find(zig_zag_ac_row);
        if length(non_zero_AC_pos) > 1
            temp = mod(non_zero_AC_pos,63);
            temp(temp==0)=63;
            shift_temp = [0;temp];
            temp = [temp;0]-[0;temp]-1;
            temp(temp<0)=temp(temp<0)+shift_temp(temp<0);
            non_zero_AC_num_preceeding_zeros = temp(1:end-1);
            
            %create permutation key
            rng(seed+k)
            permutation_key = randperm(length(non_zero_AC_num_preceeding_zeros));
            
            encrypted_zig_zag_ac_row=zeros(63,1);
            new_non_zero_AC_num_preceeding_zeros=non_zero_AC_num_preceeding_zeros(permutation_key);
            new_non_zero_AC_pos=cumsum(new_non_zero_AC_num_preceeding_zeros+1);
            encrypted_zig_zag_ac_row(new_non_zero_AC_pos)=zig_zag_ac_row(non_zero_AC_pos(permutation_key));
            e_coeff(i:i+7,j:j+7)=reshape([QC(1,1); encrypted_zig_zag_ac_row(i_z_pattern)],8,8);
        else
            e_coeff(i:i+7,j:j+7)=reshape([QC(1,1); zig_zag_ac_row(i_z_pattern)],8,8);
        end
    end
end
end

