function [e_coeff]=AC_inter_permutation(o_coeff,key)
%organize AC by block
z_pattern=[...
    9 2 3 10 17 25 18 11 4 5 12 19 26 ...
    33 41 34 27 20 13 6 7 14 21 28 35 ...
    42 49 57 50 43 36 29 22 15 8 16 23 ...
    30 37 44 51 58 59 52 45 38 31 24 32 ...
    39 46 53 60 61 54 47 40 48 55 62 63 56 64];
[~,i_z_pattern]=sort(z_pattern);

k=0;
[h,w] = size(o_coeff);
zig_zag_ac=zeros(63,h*w/64);
e_DC = zeros(h*w/64,1);
for i=1:8:h
    for j=1:8:w
        k=k+1;
        QC=o_coeff(i:i+7,j:j+7);
        zig_zag_ac(1:63,k) = QC(z_pattern);
        e_DC(k)=QC(1,1);
    end
end
%create permutation key
permutation_index=inter_permutation(1:h*w/64,key);

% permute the ACs
encrypted_zig_zag_ac=zig_zag_ac(:,permutation_index);

k=0;
e_coeff=o_coeff;
for i=1:8:h
    for j=1:8:w
        k=k+1;
        vector=encrypted_zig_zag_ac(:,k);
        e_coeff(i:i+7,j:j+7)=reshape([e_DC(k); vector(i_z_pattern)],8,8);
    end
end

end