function main
% Example file to show how compressive encryption can be applied

% Parameters
% key = secret key for encryption
% rounds = number of encryption rounds
% encryption mode = which five types of encryption to use. 0 = don't use
% and 1 = use, [substitution, DC_residual_permutation, AC_inter_permutation, AC_intra_permutation, AC_symbol_permutation]

key = 1;
rounds = 1;
encryption_mode = [1,1,1,1,1]; %use all five types

%% Compressive JPEG encrypted image
% Read the image to be encrypted
image_name='1_01_1_com_bw_org_res_nfit.JPG';
original_image=jpeg_read(image_name); 
compressive_encrypted_image=original_image;
quant_table = original_image.quant_tables{original_image.comp_info(1).quant_tbl_no};

%% Apply compressive JPEG encryption
compressive_encrypted_image.coef_arrays{1}=compressive_encrypt_jpeg(original_image.coef_arrays{1},quant_table,key,rounds,encryption_mode);
%% Apply Huffman optimization on the encrypted image
compressive_encrypted_image=bw_jpeg_optimizer_sequential(compressive_encrypted_image);

%% Save the encrypted image to results folder 
compressive_encrypted_image_name='results/compressive_encrypted.jpg';
% Delete the previously saved image
if isfile(compressive_encrypted_image_name)
    delete(compressive_encrypted_image_name)
end
jpeg_write(compressive_encrypted_image,compressive_encrypted_image_name)


%% Partial encryption case 
% Encryption applies only to specified region: region contains 2 block
% positions. Regions are separated by semi-colon
% (rectangle coordiantes top left, bottom right as a row vector)
% region=[y1-coordinate x1-coordinate y2-coordinate x2-coordinate]
% region = [20 20 40 37; 30 50 42 55 ;20 70 35 85]; this contains 3 regions
image_name='1_18_1_com_bw_org_res_nfit.JPG';
original_image=jpeg_read(image_name);
key = 1 ;
region = [20 20 40 37; 30 50 42 55 ;20 70 35 85];
[partially_encrypted_image]=encrypt_jpeg_partial(original_image,key,region);
partially_encrypted_image_name='results/partially_encrypted.jpg';
if isfile(partially_encrypted_image_name)
     delete(partially_encrypted_image_name)
end
partially_encrypted_image=bw_jpeg_optimizer_sequential(partially_encrypted_image);
jpeg_write(partially_encrypted_image,partially_encrypted_image_name)