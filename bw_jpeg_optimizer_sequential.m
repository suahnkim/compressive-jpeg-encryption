function optimized_JPEG_object=bw_jpeg_optimizer_sequential(image_object)
%due to the fact the jpeg toolbox color optimization is incorrect(or it
%is buggy), only the grayscaled version of the optimizer is provided. Use
%it as is. Also, the default is NON-PROGRESSIVE. Obviously, if the input image
%saved using progressive scanning, it will have a different effect.
%coder: Suah Kim 9/16/2020
optimized_JPEG_object = image_object;
optimized_JPEG_object.optimize_coding=1; 
optimized_JPEG_object.progressive_mode=0;
optimized_JPEG_object.image_components=1; optimized_JPEG_object.jpeg_components=1;
optimized_JPEG_object.image_color_space=1; optimized_JPEG_object.jpeg_color_space=1;
optimized_JPEG_object.comp_info=image_object.comp_info(1);
optimized_JPEG_object.comp_info.h_samp_factor=1;
optimized_JPEG_object.comp_info.v_samp_factor=1;
optimized_JPEG_object.ac_huff_tables=[];
optimized_JPEG_object.dc_huff_tables=[];
optimized_JPEG_object.quant_tables=[];
optimized_JPEG_object.quant_tables=image_object.quant_tables(image_object.comp_info(1).quant_tbl_no);
optimized_JPEG_object.coef_arrays=[];
optimized_JPEG_object.coef_arrays{1}=image_object.coef_arrays{1};
[optimized_JPEG_object.image_height,optimized_JPEG_object.image_width]=size(image_object.coef_arrays{1});

