function main_test
% Tests 20 test images from instagram compression db. Only the images
% saved as baseline (sequential, and not progressive scan) JPEG are
% tested, i.e., org images 

% Test parameters
% key = secret key for encryption
% rounds = number of encryption rounds
% encryption mode = which five types of encryption to use. 0 = don't use
% and 1 = use, [substitution, DC_residual_permutation, AC_inter_permutation, AC_intra_permutation, AC_symbol_permutation]

key = 1;
rounds = 5;
encryption_mode = [0,0,0,0,0];

image_compression_rate_compressive_encrypted=zeros(1,20);
image_compression_rate_optimized=zeros(1,20);
image_compression_rate_encrypted=zeros(1,20);

parfor image_number=1:20
    if image_number < 10
        image_name=['test_images/1_0' num2str(image_number) '_1_com_bw_org_res_nfit.JPG'];
    else
        image_name=['test_images/1_' num2str(image_number) '_1_com_bw_org_res_nfit.JPG'];
    end
    
    % Huffman-optimized image
    image_original=jpeg_read(image_name);
    [~, filename,~]=fileparts(image_name);
    optimized_image=bw_jpeg_optimizer_sequential(image_original);
    
    optimized_image_name=['encrypted_images/' filename '_opt.jpg'];
    if isfile(optimized_image_name)
        delete(optimized_image_name)
    end
    jpeg_write(optimized_image,optimized_image_name)
    
    % JPEG encrypted image (non-compressive)
    original_image=jpeg_read(optimized_image_name);
    encrypted_image=original_image;
    quant_table = original_image.quant_tables{original_image.comp_info(1).quant_tbl_no};
    encrypted_image.coef_arrays{1}= format_preserving_encryption(original_image.coef_arrays{1},quant_table,key,rounds,encryption_mode,0);
    encrypted_image=bw_jpeg_optimizer_sequential(encrypted_image);
    
    encrypted_image_name=['encrypted_images/' filename '_encrypted.jpg'];
    if isfile(encrypted_image_name)
        delete(encrypted_image_name)
    end
    jpeg_write(encrypted_image,encrypted_image_name)
    
    % Compressive JPEG encrypted image
    original_image=jpeg_read(optimized_image_name);
    compressive_encrypted_image=original_image;
    quant_table = original_image.quant_tables{original_image.comp_info(1).quant_tbl_no};
    compressive_encrypted_image.coef_arrays{1}=compressive_encrypt_jpeg(original_image.coef_arrays{1},quant_table,key,rounds,encryption_mode);
    compressive_encrypted_image=bw_jpeg_optimizer_sequential(compressive_encrypted_image);
    
    compressive_encrypted_image_name=['encrypted_images/' filename '_compressive_encrypted.jpg'];
    if isfile(compressive_encrypted_image_name)
        delete(compressive_encrypted_image_name)
    end
    jpeg_write(compressive_encrypted_image,compressive_encrypted_image_name)
    
    % Compare file sizes
    original_file=dir(image_name);
    optimized_file=dir(optimized_image_name);
    encrypted_file=dir(encrypted_image_name);
    compressive_encrypted_file=dir(compressive_encrypted_image_name);
    image_compression_rate_optimized(image_number)=(original_file.bytes/1024-optimized_file.bytes/1024)/original_file.bytes*1024*100;
    image_compression_rate_encrypted(image_number)=(original_file.bytes/1024-encrypted_file.bytes/1024)/original_file.bytes*1024*100;
    image_compression_rate_compressive_encrypted(image_number)=(original_file.bytes/1024-compressive_encrypted_file.bytes/1024)/original_file.bytes*1024*100;
end

% Plot the comparison
close all
plot(image_compression_rate_compressive_encrypted,'k')
hold on
plot(image_compression_rate_encrypted,'b')
plot(image_compression_rate_optimized,'r')

grid on
plot(1:length(image_compression_rate_compressive_encrypted), mean(image_compression_rate_compressive_encrypted)*ones(1,length(image_compression_rate_compressive_encrypted)))
pause