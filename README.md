# Compressive JPEG encryption
Author : Suah Kim, iaslab, Korea University
Last Changes: 2021-3-5

This is a beta code for the paper "Format preserving compressive JPEG encryption" submitted to IEEE SPL. 

To test how the code work, run main.m 

run main_test.m to run for all test images and for file comparison.

Thank you for checking out.

