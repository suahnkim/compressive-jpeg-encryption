function inverse_dpcm_e_coef = format_preserving_encryption(o_coef,quant_table,key,rounds,encryption_mode,dpcm_bypass_mode)
% rounds: number of encryption rounds

% encryption mode: choose which of the five encryption modes to be applied.
% [substitution, DC_residual_permutation, AC_inter_permutation, AC_intra_permutation, AC_symbol_permutation]

% dpcm_bypass_mode: 0 means don't bypass dpcm before encryption, 1 means
% bypass it before encryption

% add folder paths for encryption and decryption functions
    addpath('encryption_function');
    addpath('decryption_function');

    e_coef = o_coef;
[h, w]=size(e_coef);

% Apply dpcm before encryption, if bypass mode is 0
if dpcm_bypass_mode == 0
    [h, w]=size(e_coef);
    for i=1:8:h
        for j=1:8:w
            if i==1 && j ==1
                DC_prediction = 0;
            end
            e_coef(i,j) = o_coef(i,j)-DC_prediction;
            DC_prediction = o_coef(i,j);
        end
    end  
end

%% Encryption
% decryption check flag: 1 = enable, 0 = disable
decryption_check=0; %for debugging

for i = 1:rounds
    %% 1. encryption: substitution (s)
    if encryption_mode(1) == 1
        p_coeff = e_coef;
        [e_coef]=substitution(e_coef,quant_table,key);
        
        %Check for decryption
        if decryption_check ==1
            re_e_coeff=d_substitution(e_coef,quant_table,key);
            if isequal(re_e_coeff,p_coeff)
                disp("correctly decrypted")
            else
                disp("Incorrectly decrypted")
                pause
            end
        end
    end
    
    %% 2. encryption: DC inter_permutation (dcip)
    if encryption_mode(2) == 1
        p_coeff = e_coef;
        e_coef(1:8:h,1:8:w)=inter_permutation(e_coef(1:8:h,1:8:w),key);
        
        %Check for decryption
        if decryption_check ==1
            re_e_coeff=e_coef;
            re_e_coeff(1:8:h,1:8:w)=d_inter_permutation(e_coef(1:8:h,1:8:w),key);
            if isequal(re_e_coeff,p_coeff)
                disp("correctly decrypted")
            else
                disp("Incorrectly decrypted")
                pause
            end
        end
    end
    
    %% 3. encryption: AC inter_intra permutation (iip)
    if encryption_mode(3) == 1
        p_coeff = e_coef;
        [e_coef]=inter_intra_permutation(e_coef,key);
        
        %Check for decryption
        if decryption_check ==1
            [re_e_coeff]=d_inter_intra_permutation(e_coef,key);
            if isequal(re_e_coeff,p_coeff)
                disp("correctly decrypted")
            else
                disp("Incorrectly decrypted")
                pause
            end
        end
    end
    %% 4. encryption: AC inter_permutation (acip)
    if encryption_mode(4) == 1
        p_coeff = e_coef;
        [e_coef]=AC_inter_permutation(e_coef,key);
        
        %check for decryption
        if decryption_check ==1
            [re_e_coeff]=d_AC_inter_permutation(e_coef,key);
            if isequal(re_e_coeff,p_coeff)
                disp("correctly decrypted")
            else
                disp("Incorrectly decrypted")
                pause
            end
        end
    end
    
    %% 5. encryption: AC intra_permutation (intra)
    if encryption_mode(5) == 1
        p_coeff = e_coef;
        e_coef=intra_permutation(e_coef,key);
        
        %check for decryption
        if decryption_check ==1
            re_e_coeff=d_intra_permutation(e_coef,key);
            isequal(re_e_coeff,p_coeff)
        end
    end
end


%Pre processing to undo the effect of DPCM (jpeg_toolbox implements DPCM)
%Needs to transpose before reshaping to make sure the dc positions are aligned correctly
inverse_dpcm_e_coef=e_coef;
inverse_dpcm_e_coef(1:8:h,1:8:w)=reshape(cumsum(reshape(e_coef(1:8:h,1:8:w)',1,[])),size(e_coef(1:8:h,1:8:w),2),[])';
end