function encrypted_coef_arrays =compressive_encrypt_jpeg(coef_arrays,quant_table,key,rounds,encryption_mode)
%add folder paths for encryption and decryption functions
addpath('encryption_function');
addpath('decryption_function');

% apply prediction and dc residual coding
quantized_coef_arrays=dc_prediction(coef_arrays,quant_table);

% apply encryption
encrypted_coef_arrays = format_preserving_encryption(quantized_coef_arrays,quant_table,key,rounds,encryption_mode,1);

end





