function [p_coef]=dc_prediction(o_coef,DCTQ)
%% Prediction
[h, w]=size(o_coef);
reconstructed_pixels=zeros(h,w);
p_coef=o_coef;

% 2D dct transform in matrix multiplication form (faster calculation then 2D dct function)
dctmtx_8=dctmtx(8);

for i=1:8:h
    for j=1:8:w
        reconstructed_pixels(i:i+7,j:j+7)=round(dctmtx_8'*(o_coef(i:i+7,j:j+7).*DCTQ)*dctmtx_8)+128;
        q_block=o_coef(i:i+7,j:j+7);
        q_block(1,1)=0;
        p_reconstructed_pixels(i:i+7,j:j+7)=round(dctmtx_8'*(q_block.*DCTQ)*dctmtx_8)+128;
    end
end

counter=0;
for i=1:8:h
    for j=1:8:w
        current_p=p_reconstructed_pixels(i:i+7,j:j+7);
        if i==1 && j ==1
            %no prediction
            DC_prediction=0;
        elseif i ==1 %horizontal prediction
            %use W's right pixels to predict
            W=double(reconstructed_pixels(i:i+7,j-8:j-1));
            DC_prediction=round(sum(W(1:end,8)-current_p(1:end,1))/DCTQ(1,1));
        elseif j ==1 %vertical prediction
            %use N's bottom row pixels to predict
            N=double(reconstructed_pixels(i-8:i-1,j:j+7));
            DC_prediction=round(sum(N(8,1:end)-current_p(1,1:end))/DCTQ(1,1));
        else
            %use top + west
            N=double(reconstructed_pixels(i-8:i-1,j:j+7));
            W=double(reconstructed_pixels(i:i+7,j-8:j-1));
            DC_prediction=round((sum(W(1:end,8)-current_p(1:end,1))+sum(N(8,1:end)-current_p(1,1:end)))/DCTQ(1,1)/2);
        end
        
        counter=counter+1;
        %DC residual calculation
        p_coef(i,j)=o_coef(i,j)-DC_prediction;
    end
end
end