function e_coeff=d_substitution(e_coeff_s,DCTQ,key)
%decryption

%coefficient dependent seed creation
coeff_size=ceil(log2(abs(e_coeff_s)+1));
seed=key+sum(sum(coeff_size));
rng(seed);

%generate key stream
max_coeff_value=ceil(2*1024/min(min(DCTQ))); %Used to determine the range of quantized DCT coefficients
key_stream = randi([0,max_coeff_value],sum(sum(coeff_size>0)),1);

%encode coefficients to mod_size range 0:Z
coeff_size_pos=coeff_size>0;
mod_size=2.^coeff_size(coeff_size_pos);
e_coeff_encode=e_coeff_s(coeff_size_pos);
e_coeff_encode_neg= e_coeff_encode < mod_size/2;
e_coeff_encode(e_coeff_encode_neg)=e_coeff_encode(e_coeff_encode_neg)+mod_size(e_coeff_encode_neg)-1;

%add, modular add the coefficients with key_stream
e_coeff_mod=mod(e_coeff_encode-key_stream,mod_size);

%Re encode the coefficients to the correct range
e_coeff_temp_neg= e_coeff_mod < mod_size/2;
e_coeff_mod(e_coeff_temp_neg)=e_coeff_mod(e_coeff_temp_neg)-mod_size(e_coeff_temp_neg)+1;

%save the results
e_coeff=e_coeff_s;
e_coeff(coeff_size_pos)=e_coeff_mod;

end