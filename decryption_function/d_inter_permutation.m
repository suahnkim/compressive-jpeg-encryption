function e_coeff=d_inter_permutation(e_coeff_p,key)

%coefficient dependent seed creation
coeff_size=ceil(log2(abs(e_coeff_p)+1));
seed=key+sum(sum(coeff_size));
rng(seed); e_coeff_size=size(e_coeff_p);

%inverse permutation key for decryption
permutation_key=randperm(e_coeff_size(1)*e_coeff_size(2));
[~,invere_permutation_key]=sort(permutation_key);

e_coeff=e_coeff_p(reshape(invere_permutation_key,e_coeff_size(1), e_coeff_size(2)));
end