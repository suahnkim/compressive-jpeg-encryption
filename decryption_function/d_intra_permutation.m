function o_coeff=d_intra_permutation(e_coeff,key)
% organize AC in terms of their encoded symbols
z_pattern=[...
    9 2 3 10 17 25 18 11 4 5 12 19 26 ...
    33 41 34 27 20 13 6 7 14 21 28 35 ...
    42 49 57 50 43 36 29 22 15 8 16 23 ...
    30 37 44 51 58 59 52 45 38 31 24 32 ...
    39 46 53 60 61 54 47 40 48 55 62 63 56 64];
[~,i_z_pattern]=sort(z_pattern);

k=0;
[h,w] = size(e_coeff);
zig_zag_ac=zeros(63,h*w/64);

%seed generation
coeff_size=ceil(log2(abs(e_coeff)+1));
seed=key+sum(sum(coeff_size));

for i=1:8:h
    for j=1:8:w
        k=k+1;
        QC=e_coeff(i:i+7,j:j+7);
        encrypted_zig_zag_ac_row=reshape(QC(z_pattern),[],1);
        zig_zag_ac(1:63,k) = encrypted_zig_zag_ac_row;
        
        %apply permutation for every non-zero AC element in the block
        non_zero_AC_pos = find(encrypted_zig_zag_ac_row);
        if length(non_zero_AC_pos) > 1
            temp = mod(non_zero_AC_pos,63);
            temp(temp==0)=63;
            shift_temp = [0;temp];
            temp = [temp;0]-[0;temp]-1;
            temp(temp<0)=temp(temp<0)+shift_temp(temp<0);
            non_zero_AC_num_preceeding_zeros = temp(1:end-1);
            
            %create inverse permutation key
            rng(seed+k)
            permutation_key = randperm(length(non_zero_AC_num_preceeding_zeros));
            [~,invere_permutation_key]=sort(permutation_key);
            
            decrypted_zig_zag_ac_row=zeros(63,1);
            new_non_zero_AC_num_preceeding_zeros=non_zero_AC_num_preceeding_zeros(invere_permutation_key);
            new_non_zero_AC_pos=cumsum(new_non_zero_AC_num_preceeding_zeros+1);
            decrypted_zig_zag_ac_row(new_non_zero_AC_pos)=encrypted_zig_zag_ac_row(non_zero_AC_pos(invere_permutation_key));
            o_coeff(i:i+7,j:j+7)=reshape([QC(1,1); decrypted_zig_zag_ac_row(i_z_pattern)],8,8);
        else
            o_coeff(i:i+7,j:j+7)=reshape([QC(1,1); encrypted_zig_zag_ac_row(i_z_pattern)],8,8);
        end
    end
end

end