function [o_coeff]=d_inter_intra_permutation(e_coeff,key)
% organize AC in terms of their encoded symbols
z_pattern=[...
    9 2 3 10 17 25 18 11 4 5 12 19 26 ...
    33 41 34 27 20 13 6 7 14 21 28 35 ...
    42 49 57 50 43 36 29 22 15 8 16 23 ...
    30 37 44 51 58 59 52 45 38 31 24 32 ...
    39 46 53 60 61 54 47 40 48 55 62 63 56 64];
[~,i_z_pattern]=sort(z_pattern);

k=0;
[h,w] = size(e_coeff);
encrypted_zig_zag_ac=zeros(63,h*w/64);
e_DC = zeros(h*w/64,1);
for i=1:8:h
    for j=1:8:w
        k=k+1;
        QC=e_coeff(i:i+7,j:j+7);
        encrypted_zig_zag_ac(1:63,k) = QC(z_pattern);
        e_DC(k)=QC(1,1);
    end
end

% group symbols with same number of zero coefficients preeceding it
encrypted_zig_zag_ac_col=reshape(encrypted_zig_zag_ac,[],1);
non_zero_AC_pos = find(encrypted_zig_zag_ac_col);
temp = mod(non_zero_AC_pos,63);
temp(temp==0)=63;
shift_temp = [0;temp];
temp = [temp;0]-[0;temp]-1;
temp(temp<0)=temp(temp<0)+shift_temp(temp<0);
non_zero_AC_block_pos = temp(1:end-1);
stats_non_zero_AC_block_pos=tabulate(non_zero_AC_block_pos);
non_zero_AC_block_pos_value=stats_non_zero_AC_block_pos(:,1);

%apply permutation
for i = 1:length(non_zero_AC_block_pos_value)
    encrypted_zig_zag_ac_col(non_zero_AC_pos(non_zero_AC_block_pos == non_zero_AC_block_pos_value(i)))=d_inter_permutation(encrypted_zig_zag_ac_col(non_zero_AC_pos(non_zero_AC_block_pos == non_zero_AC_block_pos_value(i))),key+i);
end

%Save the encrypted values
decrypted_zig_zag_ac=reshape(encrypted_zig_zag_ac_col,size(encrypted_zig_zag_ac,1),size(encrypted_zig_zag_ac,2));
k=0;
o_coeff=e_coeff;
for i=1:8:h
    for j=1:8:w
        k=k+1;
        vector=decrypted_zig_zag_ac(:,k);
        o_coeff(i:i+7,j:j+7)=reshape([e_DC(k); vector(i_z_pattern)],8,8);
    end
end
end